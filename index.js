// set up
const text = 'This is a single sample of text';
const pattern = /[a-z]/i;
let pairs = [];


function isValidPair(char1, char2) {
    // validate a consecutive pair of letters that occur
    if (char1.match(pattern) !== null && char2.match(pattern) !== null) {
        // valid pair
        return char1 + char2;
    }
    return false;
}

function findPair(pair) {
    return pairs.find((item) => item.pairText === pair);
}

function incrementPairCount(pair) {
    const newPairs = pairs.map((item) => {
        if (item.pairText === pair) {
            item.pairText = pair;
            item.count += 1;
        }
    });
    return newPairs;
}

function getPairs(text) {

    // is a valid string
    if (text === undefined || text === null ||
        (text !== null && text.length === 0)) {
        return false;
    }

    const lowerText = text.toLowerCase();

    let start = 0;
    let end = lowerText.length;

    while (end > start) {
        let char1 = lowerText.charAt(start);
        let char2 = lowerText.charAt(start + 1);

        const pair = isValidPair(char1, char2);

        if (pair) {
            if (pairs.length > 0) {
                // check if pair exists
                const isInPairs = findPair(pair)

                if (isInPairs !== undefined) {
                    // increment count
                    const newPairs = incrementPairCount(pair)
                } else {
                    pairs.push({pairText: pair, count: 1});
                }
            } else {
                pairs.push({pairText: pair, count: 1});
            }

        }
        ++start;
    }
    return pairs;
}

lettersPair = getPairs(text);

lettersPair.map(letterPair => console.log(`${letterPair.pairText} => ${letterPair.count}`));
